#ifndef _FAULTY_PROC_PV_PVS_
#define _FAULTY_PROC_PV_PVS_
#include <math.h>
#include "Cpp/Performance_Variables/PerformanceVariableNode.hpp"
#include "Composed/faulty_proc2/faulty_proc2RJ__Rep1.h"
#include "Composed/faulty_proc2/faulty_proc2RJ.h"
#include "Cpp/Performance_Variables/InstantOfTime.hpp"
#include "Cpp/Performance_Variables/IntervalOfTime.hpp"
#include "Cpp/Performance_Variables/IntervalOfTimeImpulse.hpp"


class faulty_proc_PVPV0Worker:public InstantOfTime
{
 public:
  bufferSAN *buffer;
  
  faulty_proc_PVPV0Worker();
  ~faulty_proc_PVPV0Worker();
  double Reward_Function();
};

class faulty_proc_PVPV0:public PerformanceVariableNode
{
 public:
  faulty_proc2RJ *Thefaulty_proc2RJ;

  faulty_proc_PVPV0Worker *faulty_proc_PVPV0WorkerList;

  faulty_proc_PVPV0(int timeindex=0);
  ~faulty_proc_PVPV0();
  void CreateWorkerList(void);
};

class faulty_proc_PVPV1Worker:public InstantOfTime
{
 public:
  processorSAN *processor;
  
  faulty_proc_PVPV1Worker();
  ~faulty_proc_PVPV1Worker();
  double Reward_Function();
};

class faulty_proc_PVPV1:public PerformanceVariableNode
{
 public:
  faulty_proc2RJ *Thefaulty_proc2RJ;

  faulty_proc_PVPV1Worker *faulty_proc_PVPV1WorkerList;

  faulty_proc_PVPV1(int timeindex=0);
  ~faulty_proc_PVPV1();
  void CreateWorkerList(void);
};

class faulty_proc_PVPV2Worker:public InstantOfTime
{
 public:
  bufferSAN *buffer;
  
  faulty_proc_PVPV2Worker();
  ~faulty_proc_PVPV2Worker();
  double Reward_Function();
};

class faulty_proc_PVPV2:public PerformanceVariableNode
{
 public:
  faulty_proc2RJ *Thefaulty_proc2RJ;

  faulty_proc_PVPV2Worker *faulty_proc_PVPV2WorkerList;

  faulty_proc_PVPV2(int timeindex=0);
  ~faulty_proc_PVPV2();
  void CreateWorkerList(void);
};

class faulty_proc_PVPV3Worker:public InstantOfTime
{
 public:
  processorSAN *processor;
  
  faulty_proc_PVPV3Worker();
  ~faulty_proc_PVPV3Worker();
  double Reward_Function();
};

class faulty_proc_PVPV3:public PerformanceVariableNode
{
 public:
  faulty_proc2RJ *Thefaulty_proc2RJ;

  faulty_proc_PVPV3Worker *faulty_proc_PVPV3WorkerList;

  faulty_proc_PVPV3(int timeindex=0);
  ~faulty_proc_PVPV3();
  void CreateWorkerList(void);
};


class faulty_proc_PVPV4Impulse0:public IntervalOfTimeImpulse
{
 public:
  processorSAN *processor;

  faulty_proc_PVPV4Impulse0();
  ~faulty_proc_PVPV4Impulse0();
  double Impulse_Function(double);
  ImpulseNodeClass** CreateImpulseWorkerList(int);
 private:
  ImpulseNodeClass** ImpulseWorkerList;
  int ImpulseWorkerListLength;
};

class faulty_proc_PVPV4:public PerformanceVariableNode
{
 public:
  faulty_proc2RJ *Thefaulty_proc2RJ;

  faulty_proc_PVPV4Impulse0 Impulse0;

  faulty_proc_PVPV4(int timeindex=0);
  ~faulty_proc_PVPV4();
  void CreateWorkerList(void);
};

#endif
