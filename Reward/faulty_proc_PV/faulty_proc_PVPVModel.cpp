#include "faulty_proc_PVPVModel.h"

faulty_proc_PVPVModel::faulty_proc_PVPVModel(bool expandTimeArrays) {
  TheModel=new faulty_proc2RJ();
  DefineName("faulty_proc_PVPVModel");
  StateMode = 1;
  CreatePVList(5, expandTimeArrays);
  Initialize();
}



PerformanceVariableNode* faulty_proc_PVPVModel::createPVNode(int pvindex, int timeindex) {
  switch(pvindex) {
  case 0:
    return new faulty_proc_PVPV0(timeindex);
    break;
  case 1:
    return new faulty_proc_PVPV1(timeindex);
    break;
  case 2:
    return new faulty_proc_PVPV2(timeindex);
    break;
  case 3:
    return new faulty_proc_PVPV3(timeindex);
    break;
  case 4:
    return new faulty_proc_PVPV4(timeindex);
    break;
  }
  return NULL;
}
