#include "faulty_proc_PVPVNodes.h"

faulty_proc_PVPV0Worker::faulty_proc_PVPV0Worker()
{
  NumModels = 1;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&buffer);
}

faulty_proc_PVPV0Worker::~faulty_proc_PVPV0Worker() {
  delete [] TheModelPtr;
}

double faulty_proc_PVPV0Worker::Reward_Function(void) {

double reward = 0;
if (buffer->queue->Mark() < buffer->size->Mark())
     {
        reward += 1;
     }
return (reward);

return (0);



}

faulty_proc_PVPV0::faulty_proc_PVPV0(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&Thefaulty_proc2RJ);
  double startpts[1]={100.0};
  double stoppts[1]={100.0};
  Initialize("probability_non_blocking",(RewardType)0,1, startpts, stoppts, timeindex, 0,2, 2);
  AddVariableDependency("queue","buffer");
  AddVariableDependency("size","buffer");
}

faulty_proc_PVPV0::~faulty_proc_PVPV0() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void faulty_proc_PVPV0::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new faulty_proc_PVPV0Worker;
}
faulty_proc_PVPV1Worker::faulty_proc_PVPV1Worker()
{
  NumModels = 1;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&processor);
}

faulty_proc_PVPV1Worker::~faulty_proc_PVPV1Worker() {
  delete [] TheModelPtr;
}

double faulty_proc_PVPV1Worker::Reward_Function(void) {

double reward = 0;
if (processor->num_tasks->Mark() > 0 && processor->ready->Mark() == 1)
     {
        reward += 1.0 / 3;
     }
return (reward);

return (0);



}

faulty_proc_PVPV1::faulty_proc_PVPV1(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&Thefaulty_proc2RJ);
  double startpts[1]={100.0};
  double stoppts[1]={100.0};
  Initialize("utilization",(RewardType)0,1, startpts, stoppts, timeindex, 0,2, 2);
  AddVariableDependency("num_tasks","processor");
  AddVariableDependency("ready","processor");
}

faulty_proc_PVPV1::~faulty_proc_PVPV1() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void faulty_proc_PVPV1::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new faulty_proc_PVPV1Worker;
}
faulty_proc_PVPV2Worker::faulty_proc_PVPV2Worker()
{
  NumModels = 1;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&buffer);
}

faulty_proc_PVPV2Worker::~faulty_proc_PVPV2Worker() {
  delete [] TheModelPtr;
}

double faulty_proc_PVPV2Worker::Reward_Function(void) {

double reward = 0;
if (1)
     {
        reward += buffer->queue->Mark();
     }
return (reward);

return (0);



}

faulty_proc_PVPV2::faulty_proc_PVPV2(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&Thefaulty_proc2RJ);
  double startpts[1]={100.0};
  double stoppts[1]={100.0};
  Initialize("number_of_tasks_in_queue",(RewardType)0,1, startpts, stoppts, timeindex, 0,1, 1);
  AddVariableDependency("queue","buffer");
}

faulty_proc_PVPV2::~faulty_proc_PVPV2() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void faulty_proc_PVPV2::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new faulty_proc_PVPV2Worker;
}
faulty_proc_PVPV3Worker::faulty_proc_PVPV3Worker()
{
  NumModels = 1;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&processor);
}

faulty_proc_PVPV3Worker::~faulty_proc_PVPV3Worker() {
  delete [] TheModelPtr;
}

double faulty_proc_PVPV3Worker::Reward_Function(void) {

double reward = 0;
if (processor->ready->Mark() == 0)
     {
        reward += 1.0 / 3;
     }
return (reward);

return (0);



}

faulty_proc_PVPV3::faulty_proc_PVPV3(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&Thefaulty_proc2RJ);
  double startpts[1]={100.0};
  double stoppts[1]={100.0};
  Initialize("fraction_of_time_in_I_O",(RewardType)0,1, startpts, stoppts, timeindex, 0,1, 1);
  AddVariableDependency("ready","processor");
}

faulty_proc_PVPV3::~faulty_proc_PVPV3() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void faulty_proc_PVPV3::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new faulty_proc_PVPV3Worker;
}
faulty_proc_PVPV4Impulse0::faulty_proc_PVPV4Impulse0()
{
  NumberOfModelDependencies = 1;
  TheModelPtr = new BaseModelClass**[NumberOfModelDependencies];
  TheModelPtr[0] = (BaseModelClass**)(&processor);
  ImpulseWorkerListLength = 0;
}

faulty_proc_PVPV4Impulse0::~faulty_proc_PVPV4Impulse0() {
  delete [] TheModelPtr;
  if (ImpulseWorkerListLength > 0) {
    for (int ImpulseWorkerCounter = 0; ImpulseWorkerCounter < ImpulseWorkerListLength; ImpulseWorkerCounter++)
      delete ImpulseWorkerList[ImpulseWorkerCounter];
    delete[] ImpulseWorkerList;
  }
}

double faulty_proc_PVPV4Impulse0::Impulse_Function(double FiringTime)
{
return(1);

return(0);
}

ImpulseNodeClass** faulty_proc_PVPV4Impulse0::CreateImpulseWorkerList(int NumberOfWorkers) {
  ImpulseWorkerListLength = NumberOfWorkers;
  ImpulseWorkerList = new ImpulseNodeClass*[NumberOfWorkers];

  for (int ImpulseWorkerCounter = 0; ImpulseWorkerCounter < NumberOfWorkers; ImpulseWorkerCounter++)
  {
    ImpulseWorkerList[ImpulseWorkerCounter] = new faulty_proc_PVPV4Impulse0;
  }

  return ImpulseWorkerList;
}

faulty_proc_PVPV4::faulty_proc_PVPV4(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&Thefaulty_proc2RJ);
  double startpts[1]={0.0};
  double stoppts[1]={100.0};
  Initialize("number_of_tasks_processed",(RewardType)1,1, startpts, stoppts, timeindex, 1,0, 0);
  AddImpulse("Ac_I_O","processor",&Impulse0);
}

faulty_proc_PVPV4::~faulty_proc_PVPV4() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void faulty_proc_PVPV4::CreateWorkerList(void) {
}
