#ifndef _FAULTY_PROC_PV_PVMODEL_
#define _FAULTY_PROC_PV_PVMODEL_
#include "faulty_proc_PVPVNodes.h"
#include "Cpp/Performance_Variables/PVModel.hpp"
#include "Composed/faulty_proc2/faulty_proc2RJ__Rep1.h"
#include "Composed/faulty_proc2/faulty_proc2RJ.h"
class faulty_proc_PVPVModel:public PVModel {
 protected:
  PerformanceVariableNode *createPVNode(int pvindex, int timeindex);
 public:
  faulty_proc_PVPVModel(bool expandtimepoints);
};

#endif
