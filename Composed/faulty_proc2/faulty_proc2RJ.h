#ifndef faulty_proc2RJ_H_
#define faulty_proc2RJ_H_
#include <math.h>
#include "Cpp/Composer/Join.h"
#include "Cpp/Composer/AllStateVariableTypes.h"
#include "Composed/faulty_proc2/faulty_proc2RJ__Rep1.h"
#include "Atomic/buffer/bufferSAN.h"

//State variable headers
#include "Cpp/BaseClasses/SAN/Place.h"

class faulty_proc2RJ: public Join {
 public:
  faulty_proc2RJ__Rep1 * Rep1;
  bufferSAN * buffer;
  Place * queue;

  faulty_proc2RJ();
  ~faulty_proc2RJ();
};

#endif
