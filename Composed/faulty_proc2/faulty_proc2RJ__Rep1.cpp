#include "Composed/faulty_proc2/faulty_proc2RJ__Rep1.h"
char * faulty_proc2RJ__Rep1__SharedNames[] = {"queue"};

faulty_proc2RJ__Rep1::faulty_proc2RJ__Rep1():Rep("Rep1", 3, 1, faulty_proc2RJ__Rep1__SharedNames)
{
  InstanceArray = new processorSAN * [NumModels];
  delete[] ModelArray;
  ModelArray = (BaseModelClass **) InstanceArray;
  for (counter = 0; counter < NumModels; counter++)
    InstanceArray[counter] = new processorSAN();

  SetupActions();
  if (NumModels == 0) return;

  if (AllChildrenEmpty())
    NumSharedStateVariables = 0;
  else {
    //**************** Initialize local variables ****************
    queue = new Place("queue");
    addSharedPtr(queue, "queue");
    queue->ShareWith(InstanceArray[0]->queue);


    //Share state in submodels
    for (counter = 0; counter < NumModels; counter++) {
      addSharingInfo(InstanceArray[counter]->queue, queue);
    }
    for (counter = 1; counter < NumModels; counter++) {
      InstanceArray[0]->queue->ShareWith(InstanceArray[counter]->queue);
    }
  }
  Setup("processor");
}

faulty_proc2RJ__Rep1::~faulty_proc2RJ__Rep1() {
  if (NumModels == 0) return;
  delete queue;
  for (int i = 0; i < NumModels; i++)
    delete InstanceArray[i];
}

