#ifndef faulty_proc2RJ__Rep1_H_
#define faulty_proc2RJ__Rep1_H_
#include <math.h>
#include "Cpp/Composer/Rep.h"
#include "Cpp/Composer/AllStateVariableTypes.h"

//Submodel header files
#include "Atomic/processor/processorSAN.h"

//State variable headers
#include "Cpp/BaseClasses/SAN/Place.h"

class faulty_proc2RJ__Rep1: public Rep {
 public:
  processorSAN ** InstanceArray;

  faulty_proc2RJ__Rep1();
  ~faulty_proc2RJ__Rep1();

  // Declare new variables
  Place * queue;
};

#endif
