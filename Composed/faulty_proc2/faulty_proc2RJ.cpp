#include "Composed/faulty_proc2/faulty_proc2RJ.h"
char * faulty_proc2RJ__SharedNames[] = {"queue"};

faulty_proc2RJ::faulty_proc2RJ():Join("Join1", 2, 1,faulty_proc2RJ__SharedNames) {
  Rep1 = new faulty_proc2RJ__Rep1();
  ModelArray[0] = (BaseModelClass*) Rep1;
  ModelArray[0]->DefineName("Rep1");
  buffer = new bufferSAN();
  ModelArray[1] = (BaseModelClass*) buffer;
  ModelArray[1]->DefineName("buffer");

  SetupActions();
  if (AllChildrenEmpty())
    NumSharedStateVariables = 0;
  else {
    //**************  State sharing info  **************
    //Shared variable 0
    queue = new Place("queue");
    addSharedPtr(queue, "queue" );
    if (Rep1->NumStateVariables > 0) {
      queue->ShareWith(getSharableSVPointer(Rep1->queue));
      addSharingInfo(getSharableSVPointer(Rep1->queue), queue, Rep1);
    }
    if (buffer->NumStateVariables > 0) {
      queue->ShareWith(getSharableSVPointer(buffer->queue));
      addSharingInfo(getSharableSVPointer(buffer->queue), queue, buffer);
    }

  }

  Setup();
}

faulty_proc2RJ::~faulty_proc2RJ() {
  if (!AllChildrenEmpty()) {
    delete queue;
  }
  delete Rep1;
  delete buffer;
}
