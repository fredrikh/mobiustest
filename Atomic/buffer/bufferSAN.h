#ifndef bufferSAN_H_
#define bufferSAN_H_

#include "Cpp/BaseClasses/EmptyGroup.h"
#include "Cpp/BaseClasses/GlobalVariables.h"
#include "Cpp/BaseClasses/PreselectGroup.h"
#include "Cpp/BaseClasses/PostselectGroup.h"
#include "Cpp/BaseClasses/state/StructStateVariable.h"
#include "Cpp/BaseClasses/state/ArrayStateVariable.h"
#include "Cpp/BaseClasses/SAN/SANModel.h" 
#include "Cpp/BaseClasses/SAN/Place.h"
#include "Cpp/BaseClasses/SAN/ExtendedPlace.h"
extern Double arr_rate;
extern UserDistributions* TheDistribution;

void MemoryError();


/*********************************************************************
               bufferSAN Submodel Definition                   
*********************************************************************/

class bufferSAN:public SANModel{
public:

class Ac_arrivalActivity:public Activity {
public:

  Place* queue;
  short* queue_Mobius_Mark;
  Place* size;
  short* size_Mobius_Mark;

  double* TheDistributionParameters;
  Ac_arrivalActivity();
  ~Ac_arrivalActivity();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // Ac_arrivalActivityActivity

  //List of user-specified place names
  Place* size;
  Place* queue;

  // Create instances of all actvities
  Ac_arrivalActivity Ac_arrival;
  //Create instances of all groups 
  EmptyGroup ImmediateGroup;

  bufferSAN();
  ~bufferSAN();
  void CustomInitialization();

  void assignPlacesToActivitiesInst();
  void assignPlacesToActivitiesTimed();
}; // end bufferSAN

#endif // bufferSAN_H_
