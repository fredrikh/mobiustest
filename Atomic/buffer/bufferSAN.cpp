

// This C++ file was created by SanEditor

#include "Atomic/buffer/bufferSAN.h"

#include <stdlib.h>
#include <iostream>

#include <math.h>


/*****************************************************************
                         bufferSAN Constructor             
******************************************************************/


bufferSAN::bufferSAN(){


  Activity* InitialActionList[1]={
    &Ac_arrival  // 0
  };

  BaseGroupClass* InitialGroupList[1]={
    (BaseGroupClass*) &(Ac_arrival)
  };

  size = new Place("size" ,5);
  queue = new Place("queue" ,0);
  BaseStateVariableClass* InitialPlaces[2]={
    size,  // 0
    queue   // 1
  };
  BaseStateVariableClass* InitialROPlaces[0]={
  };
  initializeSANModelNow("buffer", 2, InitialPlaces, 
                        0, InitialROPlaces, 
                        1, InitialActionList, 1, InitialGroupList);


  assignPlacesToActivitiesInst();
  assignPlacesToActivitiesTimed();

  int AffectArcs[1][2]={ 
    {1,0}
  };
  for(int n=0;n<1;n++) {
    AddAffectArc(InitialPlaces[AffectArcs[n][0]],
                 InitialActionList[AffectArcs[n][1]]);
  }
  int EnableArcs[2][2]={ 
    {1,0}, {0,0}
  };
  for(int n=0;n<2;n++) {
    AddEnableArc(InitialPlaces[EnableArcs[n][0]],
                 InitialActionList[EnableArcs[n][1]]);
  }

  for(int n=0;n<1;n++) {
    InitialActionList[n]->LinkVariables();
  }
  CustomInitialization();

}

void bufferSAN::CustomInitialization() {

}
bufferSAN::~bufferSAN(){
  for (int i = 0; i < NumStateVariables-NumReadOnlyPlaces; i++)
    delete LocalStateVariables[i];
};

void bufferSAN::assignPlacesToActivitiesInst(){
}
void bufferSAN::assignPlacesToActivitiesTimed(){
  Ac_arrival.queue = (Place*) LocalStateVariables[1];
  Ac_arrival.size = (Place*) LocalStateVariables[0];
}
/*****************************************************************/
/*                  Activity Method Definitions                  */
/*****************************************************************/

/*======================Ac_arrivalActivity========================*/

bufferSAN::Ac_arrivalActivity::Ac_arrivalActivity(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("Ac_arrival",0,Exponential, RaceEnabled, 1,2, false);
}

bufferSAN::Ac_arrivalActivity::~Ac_arrivalActivity(){
  delete[] TheDistributionParameters;
}

void bufferSAN::Ac_arrivalActivity::LinkVariables(){
  queue->Register(&queue_Mobius_Mark);
  size->Register(&size_Mobius_Mark);
}

bool bufferSAN::Ac_arrivalActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=((/* has the buffer capacity been reached? */
queue->Mark() < size->Mark()));
  return NewEnabled;
}

double bufferSAN::Ac_arrivalActivity::Rate(){
  return arr_rate;
  return 1.0;  // default rate if none is specified
}

double bufferSAN::Ac_arrivalActivity::Weight(){ 
  return 1;
}

bool bufferSAN::Ac_arrivalActivity::ReactivationPredicate(){ 
  return false;
}

bool bufferSAN::Ac_arrivalActivity::ReactivationFunction(){ 
  return false;
}

double bufferSAN::Ac_arrivalActivity::SampleDistribution(){
  return TheDistribution->Exponential(arr_rate);
}

double* bufferSAN::Ac_arrivalActivity::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int bufferSAN::Ac_arrivalActivity::Rank(){
  return 1;
}

BaseActionClass* bufferSAN::Ac_arrivalActivity::Fire(){
  /* do nothing */
;
  (*(queue_Mobius_Mark))++;
  return this;
}

