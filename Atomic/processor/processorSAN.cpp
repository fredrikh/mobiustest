

// This C++ file was created by SanEditor

#include "Atomic/processor/processorSAN.h"

#include <stdlib.h>
#include <iostream>

#include <math.h>


/*****************************************************************
                         processorSAN Constructor             
******************************************************************/


processorSAN::processorSAN(){


  Ac_processingGroup.initialize(3, "Ac_processingGroup");
  Ac_processingGroup.appendGroup((BaseGroupClass*) &Ac_processing_case1);
  Ac_processingGroup.appendGroup((BaseGroupClass*) &Ac_processing_case2);
  Ac_processingGroup.appendGroup((BaseGroupClass*) &Ac_processing_case3);

  Activity* InitialActionList[5]={
    &Ac_I_O, //0
    &Ac_processing_case1, //1
    &Ac_processing_case2, //2
    &Ac_processing_case3, //3
    &Ac_access  // 4
  };

  BaseGroupClass* InitialGroupList[3]={
    (BaseGroupClass*) &(Ac_I_O), 
    (BaseGroupClass*) &(Ac_processingGroup), 
    (BaseGroupClass*) &(Ac_access)
  };

  ready = new Place("ready" ,1);
  done = new Place("done" ,0);
  num_tasks = new Place("num_tasks" ,0);
  queue = new Place("queue" ,0);
  BaseStateVariableClass* InitialPlaces[4]={
    ready,  // 0
    done,  // 1
    num_tasks,  // 2
    queue   // 3
  };
  BaseStateVariableClass* InitialROPlaces[0]={
  };
  initializeSANModelNow("processor", 4, InitialPlaces, 
                        0, InitialROPlaces, 
                        5, InitialActionList, 3, InitialGroupList);


  assignPlacesToActivitiesInst();
  assignPlacesToActivitiesTimed();

  int AffectArcs[12][2]={ 
    {1,0}, {0,0}, {2,1}, {0,1}, {1,1}, {2,2}, {0,2}, {1,2}, {2,3}, 
    {0,3}, {3,4}, {2,4}
  };
  for(int n=0;n<12;n++) {
    AddAffectArc(InitialPlaces[AffectArcs[n][0]],
                 InitialActionList[AffectArcs[n][1]]);
  }
  int EnableArcs[9][2]={ 
    {1,0}, {2,1}, {0,1}, {2,2}, {0,2}, {2,3}, {0,3}, {3,4}, {2,4}
  };
  for(int n=0;n<9;n++) {
    AddEnableArc(InitialPlaces[EnableArcs[n][0]],
                 InitialActionList[EnableArcs[n][1]]);
  }

  for(int n=0;n<5;n++) {
    InitialActionList[n]->LinkVariables();
  }
  CustomInitialization();

}

void processorSAN::CustomInitialization() {

}
processorSAN::~processorSAN(){
  for (int i = 0; i < NumStateVariables-NumReadOnlyPlaces; i++)
    delete LocalStateVariables[i];
};

void processorSAN::assignPlacesToActivitiesInst(){
}
void processorSAN::assignPlacesToActivitiesTimed(){
  Ac_I_O.done = (Place*) LocalStateVariables[1];
  Ac_I_O.ready = (Place*) LocalStateVariables[0];
  Ac_processing_case1.num_tasks = (Place*) LocalStateVariables[2];
  Ac_processing_case1.ready = (Place*) LocalStateVariables[0];
  Ac_processing_case1.done = (Place*) LocalStateVariables[1];
  Ac_processing_case2.num_tasks = (Place*) LocalStateVariables[2];
  Ac_processing_case2.ready = (Place*) LocalStateVariables[0];
  Ac_processing_case2.done = (Place*) LocalStateVariables[1];
  Ac_processing_case3.num_tasks = (Place*) LocalStateVariables[2];
  Ac_processing_case3.ready = (Place*) LocalStateVariables[0];
  Ac_access.queue = (Place*) LocalStateVariables[3];
  Ac_access.num_tasks = (Place*) LocalStateVariables[2];
}
/*****************************************************************/
/*                  Activity Method Definitions                  */
/*****************************************************************/

/*======================Ac_I_OActivity========================*/

processorSAN::Ac_I_OActivity::Ac_I_OActivity(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("Ac_I_O",0,Exponential, RaceEnabled, 2,1, false);
}

processorSAN::Ac_I_OActivity::~Ac_I_OActivity(){
  delete[] TheDistributionParameters;
}

void processorSAN::Ac_I_OActivity::LinkVariables(){
  done->Register(&done_Mobius_Mark);
  ready->Register(&ready_Mobius_Mark);
}

bool processorSAN::Ac_I_OActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(done_Mobius_Mark)) >=1));
  return NewEnabled;
}

double processorSAN::Ac_I_OActivity::Rate(){
  return io_rate;
  return 1.0;  // default rate if none is specified
}

double processorSAN::Ac_I_OActivity::Weight(){ 
  return 1;
}

bool processorSAN::Ac_I_OActivity::ReactivationPredicate(){ 
  return false;
}

bool processorSAN::Ac_I_OActivity::ReactivationFunction(){ 
  return false;
}

double processorSAN::Ac_I_OActivity::SampleDistribution(){
  return TheDistribution->Exponential(io_rate);
}

double* processorSAN::Ac_I_OActivity::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int processorSAN::Ac_I_OActivity::Rank(){
  return 1;
}

BaseActionClass* processorSAN::Ac_I_OActivity::Fire(){
  (*(done_Mobius_Mark))--;
  /* when I/O done, reset ready */
if (done->Mark() == 0)
  ready->Mark() = 1;
  return this;
}

/*======================Ac_processingActivity_case1========================*/

processorSAN::Ac_processingActivity_case1::Ac_processingActivity_case1(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("Ac_processing_case1",1,Exponential, RaceEnabled, 3,2, false);
}

processorSAN::Ac_processingActivity_case1::~Ac_processingActivity_case1(){
  delete[] TheDistributionParameters;
}

void processorSAN::Ac_processingActivity_case1::LinkVariables(){
  num_tasks->Register(&num_tasks_Mobius_Mark);
  ready->Register(&ready_Mobius_Mark);
  done->Register(&done_Mobius_Mark);
}

bool processorSAN::Ac_processingActivity_case1::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(num_tasks_Mobius_Mark)) >=1)&&((*(ready_Mobius_Mark)) >=1));
  return NewEnabled;
}

double processorSAN::Ac_processingActivity_case1::Rate(){
  return proc_rate;
  return 1.0;  // default rate if none is specified
}

double processorSAN::Ac_processingActivity_case1::Weight(){ 
  if (num_tasks->Mark() == 1)
  return(1.0);
else return(ok_prob);
}

bool processorSAN::Ac_processingActivity_case1::ReactivationPredicate(){ 
  return false;
}

bool processorSAN::Ac_processingActivity_case1::ReactivationFunction(){ 
  return false;
}

double processorSAN::Ac_processingActivity_case1::SampleDistribution(){
  return TheDistribution->Exponential(proc_rate);
}

double* processorSAN::Ac_processingActivity_case1::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int processorSAN::Ac_processingActivity_case1::Rank(){
  return 1;
}

BaseActionClass* processorSAN::Ac_processingActivity_case1::Fire(){
  (*(num_tasks_Mobius_Mark))--;
  (*(ready_Mobius_Mark))--;
  /* put one or two tasks in done, clear num_tasks */
done->Mark() = num_tasks->Mark() + 1;
num_tasks->Mark() = 0;
  return this;
}

/*======================Ac_processingActivity_case2========================*/

processorSAN::Ac_processingActivity_case2::Ac_processingActivity_case2(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("Ac_processing_case2",1,Exponential, RaceEnabled, 3,2, false);
}

processorSAN::Ac_processingActivity_case2::~Ac_processingActivity_case2(){
  delete[] TheDistributionParameters;
}

void processorSAN::Ac_processingActivity_case2::LinkVariables(){
  num_tasks->Register(&num_tasks_Mobius_Mark);
  ready->Register(&ready_Mobius_Mark);
  done->Register(&done_Mobius_Mark);
}

bool processorSAN::Ac_processingActivity_case2::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(num_tasks_Mobius_Mark)) >=1)&&((*(ready_Mobius_Mark)) >=1));
  return NewEnabled;
}

double processorSAN::Ac_processingActivity_case2::Rate(){
  return proc_rate;
  return 1.0;  // default rate if none is specified
}

double processorSAN::Ac_processingActivity_case2::Weight(){ 
  if (num_tasks->Mark() == 1)
  return(0);
else return(one_error_prob);
}

bool processorSAN::Ac_processingActivity_case2::ReactivationPredicate(){ 
  return false;
}

bool processorSAN::Ac_processingActivity_case2::ReactivationFunction(){ 
  return false;
}

double processorSAN::Ac_processingActivity_case2::SampleDistribution(){
  return TheDistribution->Exponential(proc_rate);
}

double* processorSAN::Ac_processingActivity_case2::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int processorSAN::Ac_processingActivity_case2::Rank(){
  return 1;
}

BaseActionClass* processorSAN::Ac_processingActivity_case2::Fire(){
  (*(num_tasks_Mobius_Mark))--;
  (*(ready_Mobius_Mark))--;
  (*(done_Mobius_Mark))++;
  return this;
}

/*======================Ac_processingActivity_case3========================*/

processorSAN::Ac_processingActivity_case3::Ac_processingActivity_case3(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("Ac_processing_case3",1,Exponential, RaceEnabled, 2,2, false);
}

processorSAN::Ac_processingActivity_case3::~Ac_processingActivity_case3(){
  delete[] TheDistributionParameters;
}

void processorSAN::Ac_processingActivity_case3::LinkVariables(){
  num_tasks->Register(&num_tasks_Mobius_Mark);
  ready->Register(&ready_Mobius_Mark);
}

bool processorSAN::Ac_processingActivity_case3::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(num_tasks_Mobius_Mark)) >=1)&&((*(ready_Mobius_Mark)) >=1));
  return NewEnabled;
}

double processorSAN::Ac_processingActivity_case3::Rate(){
  return proc_rate;
  return 1.0;  // default rate if none is specified
}

double processorSAN::Ac_processingActivity_case3::Weight(){ 
  if (num_tasks->Mark() == 1)
  return(0);
else return(1.0 - ok_prob - one_error_prob);
}

bool processorSAN::Ac_processingActivity_case3::ReactivationPredicate(){ 
  return false;
}

bool processorSAN::Ac_processingActivity_case3::ReactivationFunction(){ 
  return false;
}

double processorSAN::Ac_processingActivity_case3::SampleDistribution(){
  return TheDistribution->Exponential(proc_rate);
}

double* processorSAN::Ac_processingActivity_case3::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int processorSAN::Ac_processingActivity_case3::Rank(){
  return 1;
}

BaseActionClass* processorSAN::Ac_processingActivity_case3::Fire(){
  (*(num_tasks_Mobius_Mark))--;
  (*(ready_Mobius_Mark))--;
  (*(ready_Mobius_Mark))++;
  (*(num_tasks_Mobius_Mark))++;
  return this;
}

/*======================Ac_accessActivity========================*/

processorSAN::Ac_accessActivity::Ac_accessActivity(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("Ac_access",2,Exponential, RaceEnabled, 2,2, false);
}

processorSAN::Ac_accessActivity::~Ac_accessActivity(){
  delete[] TheDistributionParameters;
}

void processorSAN::Ac_accessActivity::LinkVariables(){
  queue->Register(&queue_Mobius_Mark);
  num_tasks->Register(&num_tasks_Mobius_Mark);
}

bool processorSAN::Ac_accessActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(queue_Mobius_Mark)) >=1)&&(num_tasks->Mark() < 2));
  return NewEnabled;
}

double processorSAN::Ac_accessActivity::Rate(){
  return access_rate;
  return 1.0;  // default rate if none is specified
}

double processorSAN::Ac_accessActivity::Weight(){ 
  return 1;
}

bool processorSAN::Ac_accessActivity::ReactivationPredicate(){ 
  return false;
}

bool processorSAN::Ac_accessActivity::ReactivationFunction(){ 
  return false;
}

double processorSAN::Ac_accessActivity::SampleDistribution(){
  return TheDistribution->Exponential(access_rate);
}

double* processorSAN::Ac_accessActivity::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int processorSAN::Ac_accessActivity::Rank(){
  return 1;
}

BaseActionClass* processorSAN::Ac_accessActivity::Fire(){
  /* do nothing */
;
  (*(queue_Mobius_Mark))--;
  (*(num_tasks_Mobius_Mark))++;
  return this;
}

