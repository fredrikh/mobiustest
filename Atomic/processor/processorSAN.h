#ifndef processorSAN_H_
#define processorSAN_H_

#include "Cpp/BaseClasses/EmptyGroup.h"
#include "Cpp/BaseClasses/GlobalVariables.h"
#include "Cpp/BaseClasses/PreselectGroup.h"
#include "Cpp/BaseClasses/PostselectGroup.h"
#include "Cpp/BaseClasses/state/StructStateVariable.h"
#include "Cpp/BaseClasses/state/ArrayStateVariable.h"
#include "Cpp/BaseClasses/SAN/SANModel.h" 
#include "Cpp/BaseClasses/SAN/Place.h"
#include "Cpp/BaseClasses/SAN/ExtendedPlace.h"
extern Double access_rate;
extern Double ok_prob;
extern Double one_error_prob;
extern Double proc_rate;
extern Double io_rate;
extern UserDistributions* TheDistribution;

void MemoryError();


/*********************************************************************
               processorSAN Submodel Definition                   
*********************************************************************/

class processorSAN:public SANModel{
public:

class Ac_I_OActivity:public Activity {
public:

  Place* done;
  short* done_Mobius_Mark;
  Place* ready;
  short* ready_Mobius_Mark;

  double* TheDistributionParameters;
  Ac_I_OActivity();
  ~Ac_I_OActivity();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // Ac_I_OActivityActivity

class Ac_processingActivity_case1:public Activity {
public:

  Place* num_tasks;
  short* num_tasks_Mobius_Mark;
  Place* ready;
  short* ready_Mobius_Mark;
  Place* done;
  short* done_Mobius_Mark;

  double* TheDistributionParameters;
  Ac_processingActivity_case1();
  ~Ac_processingActivity_case1();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // Ac_processingActivity_case1Activity

class Ac_processingActivity_case2:public Activity {
public:

  Place* num_tasks;
  short* num_tasks_Mobius_Mark;
  Place* ready;
  short* ready_Mobius_Mark;
  Place* done;
  short* done_Mobius_Mark;

  double* TheDistributionParameters;
  Ac_processingActivity_case2();
  ~Ac_processingActivity_case2();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // Ac_processingActivity_case2Activity

class Ac_processingActivity_case3:public Activity {
public:

  Place* num_tasks;
  short* num_tasks_Mobius_Mark;
  Place* ready;
  short* ready_Mobius_Mark;

  double* TheDistributionParameters;
  Ac_processingActivity_case3();
  ~Ac_processingActivity_case3();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // Ac_processingActivity_case3Activity

class Ac_accessActivity:public Activity {
public:

  Place* queue;
  short* queue_Mobius_Mark;
  Place* num_tasks;
  short* num_tasks_Mobius_Mark;

  double* TheDistributionParameters;
  Ac_accessActivity();
  ~Ac_accessActivity();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // Ac_accessActivityActivity

  //List of user-specified place names
  Place* ready;
  Place* done;
  Place* num_tasks;
  Place* queue;

  // Create instances of all actvities
  Ac_I_OActivity Ac_I_O;
  Ac_processingActivity_case1 Ac_processing_case1;
  Ac_processingActivity_case2 Ac_processing_case2;
  Ac_processingActivity_case3 Ac_processing_case3;
  Ac_accessActivity Ac_access;
  //Create instances of all groups 
  EmptyGroup ImmediateGroup;
  PostselectGroup Ac_processingGroup;
  processorSAN();
  ~processorSAN();
  void CustomInitialization();

  void assignPlacesToActivitiesInst();
  void assignPlacesToActivitiesTimed();
}; // end processorSAN

#endif // processorSAN_H_
