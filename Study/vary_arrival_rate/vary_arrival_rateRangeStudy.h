
#ifndef vary_arrival_rateRangeSTUDY_H_
#define vary_arrival_rateRangeSTUDY_H_

#include "Reward/faulty_proc_PV/faulty_proc_PVPVNodes.h"
#include "Reward/faulty_proc_PV/faulty_proc_PVPVModel.h"
#include "Cpp/BaseClasses/GlobalVariables.h"
#include "Cpp/Study/BaseStudyClass.hpp"

extern Double access_rate;
extern Double arr_rate;
extern Double io_rate;
extern Double ok_prob;
extern Double one_error_prob;
extern Double proc_rate;

class vary_arrival_rateRangeStudy : public BaseStudyClass {
public:

vary_arrival_rateRangeStudy();
~vary_arrival_rateRangeStudy();

private:

double *access_rateValues;
double *arr_rateValues;
double *io_rateValues;
double *ok_probValues;
double *one_error_probValues;
double *proc_rateValues;

void SetValues_access_rate();
void SetValues_arr_rate();
void SetValues_io_rate();
void SetValues_ok_prob();
void SetValues_one_error_prob();
void SetValues_proc_rate();

void PrintGlobalValues(int);
void *GetGVValue(char *TheGVName);
void OverrideGVValue(char *TheGVName, void *TheGVValue);
void SetGVs(int expnum);
PVModel *GetPVModel(bool expandTimeArrays);
};

#endif

