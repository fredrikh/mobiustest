
#include "Study/vary_error_prob/vary_error_probSetStudy.h"

//******************************************************
//Global Variables
//******************************************************
Double access_rate;
Double arr_rate;
Double io_rate;
Double ok_prob;
Double one_error_prob;
Double proc_rate;

//********************************************************
//vary_error_probSetStudy Constructor
//********************************************************
vary_error_probSetStudy::vary_error_probSetStudy() {

  // define arrays of global variable names and types
  NumGVs = 6;
  NumExps = 2;

  GVNames = new char*[NumGVs];
  GVTypes = new char*[NumGVs];
  GVNames[0]=strdup("access_rate");
  GVTypes[0]=strdup("double");
  GVNames[1]=strdup("arr_rate");
  GVTypes[1]=strdup("double");
  GVNames[2]=strdup("io_rate");
  GVTypes[2]=strdup("double");
  GVNames[3]=strdup("ok_prob");
  GVTypes[3]=strdup("double");
  GVNames[4]=strdup("one_error_prob");
  GVTypes[4]=strdup("double");
  GVNames[5]=strdup("proc_rate");
  GVTypes[5]=strdup("double");

  // create the arrays to store the values of each gv
  access_rateValues = new double[NumExps];
  arr_rateValues = new double[NumExps];
  io_rateValues = new double[NumExps];
  ok_probValues = new double[NumExps];
  one_error_probValues = new double[NumExps];
  proc_rateValues = new double[NumExps];

  // call methods to assign values to each gv
  SetValues_access_rate();
  SetValues_arr_rate();
  SetValues_io_rate();
  SetValues_ok_prob();
  SetValues_one_error_prob();
  SetValues_proc_rate();
  SetDefaultMobiusRoot(MOBIUSROOT);
}


//******************************************************
//vary_error_probSetStudy Destructor
//******************************************************
vary_error_probSetStudy::~vary_error_probSetStudy() {
  delete [] access_rateValues;
  delete [] arr_rateValues;
  delete [] io_rateValues;
  delete [] ok_probValues;
  delete [] one_error_probValues;
  delete [] proc_rateValues;
  delete ThePVModel;
}


//******************************************************
// set values for access_rate
//******************************************************
void vary_error_probSetStudy::SetValues_access_rate() {
  access_rateValues[0] = 20;
  access_rateValues[1] = 20;
}


//******************************************************
// set values for arr_rate
//******************************************************
void vary_error_probSetStudy::SetValues_arr_rate() {
  arr_rateValues[0] = 10;
  arr_rateValues[1] = 10;
}


//******************************************************
// set values for io_rate
//******************************************************
void vary_error_probSetStudy::SetValues_io_rate() {
  io_rateValues[0] = 10;
  io_rateValues[1] = 7.5;
}


//******************************************************
// set values for ok_prob
//******************************************************
void vary_error_probSetStudy::SetValues_ok_prob() {
  ok_probValues[0] = 0.75;
  ok_probValues[1] = 0.77;
}


//******************************************************
// set values for one_error_prob
//******************************************************
void vary_error_probSetStudy::SetValues_one_error_prob() {
  one_error_probValues[0] = 0.24;
  one_error_probValues[1] = 0.21;
}


//******************************************************
// set values for proc_rate
//******************************************************
void vary_error_probSetStudy::SetValues_proc_rate() {
  proc_rateValues[0] = 1;
  proc_rateValues[1] = 1;
}



//******************************************************
//print values of gv (for debugging)
//******************************************************
void vary_error_probSetStudy::PrintGlobalValues(int expNum) {
  if (NumGVs == 0) {
    cout<<"There are no global variables."<<endl;
    return;
  }

  SetGVs(expNum);

  cout<<"The Global Variable values for experiment "<<
    GetExpName(expNum)<<" are:"<<endl;
  cout << "access_rate\tdouble\t" << access_rate << endl;
  cout << "arr_rate\tdouble\t" << arr_rate << endl;
  cout << "io_rate\tdouble\t" << io_rate << endl;
  cout << "ok_prob\tdouble\t" << ok_prob << endl;
  cout << "one_error_prob\tdouble\t" << one_error_prob << endl;
  cout << "proc_rate\tdouble\t" << proc_rate << endl;
}


//******************************************************
//retrieve the value of a global variable
//******************************************************
void *vary_error_probSetStudy::GetGVValue(char *TheGVName) {
  if (strcmp("access_rate", TheGVName) == 0)
    return &access_rate;
  else if (strcmp("arr_rate", TheGVName) == 0)
    return &arr_rate;
  else if (strcmp("io_rate", TheGVName) == 0)
    return &io_rate;
  else if (strcmp("ok_prob", TheGVName) == 0)
    return &ok_prob;
  else if (strcmp("one_error_prob", TheGVName) == 0)
    return &one_error_prob;
  else if (strcmp("proc_rate", TheGVName) == 0)
    return &proc_rate;
  else 
    cerr<<"!! vary_error_probSetStudy::GetGVValue: Global Variable "<<TheGVName<<" does not exist."<<endl;
  return NULL;
}


//******************************************************
//override the value of a global variable
//******************************************************
void vary_error_probSetStudy::OverrideGVValue(char *TheGVName,void *TheGVValue) {
  if (strcmp("access_rate", TheGVName) == 0)
    SetGvValue(access_rate, *(double *)TheGVValue);
  else if (strcmp("arr_rate", TheGVName) == 0)
    SetGvValue(arr_rate, *(double *)TheGVValue);
  else if (strcmp("io_rate", TheGVName) == 0)
    SetGvValue(io_rate, *(double *)TheGVValue);
  else if (strcmp("ok_prob", TheGVName) == 0)
    SetGvValue(ok_prob, *(double *)TheGVValue);
  else if (strcmp("one_error_prob", TheGVName) == 0)
    SetGvValue(one_error_prob, *(double *)TheGVValue);
  else if (strcmp("proc_rate", TheGVName) == 0)
    SetGvValue(proc_rate, *(double *)TheGVValue);
  else 
    cerr<<"!! vary_error_probSetStudy::OverrideGVValue: Global Variable "<<TheGVName<<" does not exist."<<endl;
}


//******************************************************
//set the value of all global variables to the given exp
//******************************************************
void vary_error_probSetStudy::SetGVs(int expNum) {
  SetGvValue(access_rate, access_rateValues[expNum]);
  SetGvValue(arr_rate, arr_rateValues[expNum]);
  SetGvValue(io_rate, io_rateValues[expNum]);
  SetGvValue(ok_prob, ok_probValues[expNum]);
  SetGvValue(one_error_prob, one_error_probValues[expNum]);
  SetGvValue(proc_rate, proc_rateValues[expNum]);
}


//******************************************************
//static class method called by solvers to create study 
//(and thus create all of the model)
//******************************************************
BaseStudyClass* GlobalStudyPtr = NULL;
BaseStudyClass * GenerateStudy() {
  if (GlobalStudyPtr == NULL)
    GlobalStudyPtr = new vary_error_probSetStudy();
  return GlobalStudyPtr;
}

void DestructStudy() {
  delete GlobalStudyPtr;
  GlobalStudyPtr = NULL;
}
//******************************************************
//get and create the PVModel
//******************************************************
PVModel* vary_error_probSetStudy::GetPVModel(bool expandTimeArrays) {
  if (ThePVModel!=NULL)
    delete ThePVModel;
  // create the PV model
  ThePVModel=new faulty_proc_PVPVModel(expandTimeArrays);
  return ThePVModel;
}


